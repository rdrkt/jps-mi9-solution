jps-mi9-solution
================

A basic json filtering mechanism that conforms to the specifications set forth on mi9-coding-challenge.herokuapp.com.

Usage
-----

    node index.js

Post the JSON document to the server running on port 5000 (If running locally).  If deployed to heroku, post data to port 80.

Testing
-------

The nodeunit package is included in package.json, but if you have it installed globally you can execute the tests by calling:

    nodeunit test
    
If you wish to try a simple test from Chrome, you can use the console to load the root url of the project 
(eg: jps-mi9-solution.herokuapp.com), use the following example:

	var obj = {"payload": [{}, {drm:true, episodeCount:10}, {}]};
	var req = new XMLHttpRequest();
	req.open('POST', 'http://jps-mi9-solution.herokuapp.com/');
	req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	req.send(JSON.stringify(obj));

Core files
----------
  - ``index.js`` - Includes the basic express server to handle connections and responses in JSON
  - ``mi9.js`` - Includes the logic used to filter the results and select the key/value pairs for response