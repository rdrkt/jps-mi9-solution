var mi9 = {
  /**
	* defaultShow contains the data we expect to always be available on every show. This should 
	* include both values you intend to filter against and values you wish to return after filtering
	*/
  defaultShow: {
	"title": null,
	"slug": null,
	"image": {
	   "showImage": null
    },
    "episodeCount": 0,
    "drm": false
  },
		
  /**
   * normalize ensures that the values used in a filter or returnValues function exist
   * prior to executing the functions
   *
   * @param obj object
   * @param defaults object
   * @returns object
   */
  normalize: function(obj, defaults){
    var out = JSON.parse(JSON.stringify(defaults)); //nonsense to fake pass by reference

	for(i in obj){
      out[i] = obj[i];
    }
	
	return out;
  },
  
  /**
   * filter an array of show objects. 
   * 
   * @param shows array
   * @param filter function
   * @param returnValues function
   * @returns array
   */
  filterShows: function(shows, filter, returnValues){
	filteredArray = [];
	
	for(i in shows){
      var show = shows[i];
      show = this.normalize(show, this.defaultShow);
      
      if(filter(show)){
    	if(returnValues){
          filteredArray.push( returnValues(show) );
    	} else {
    	  filteredArray.push( show );
    	}
      }
    }
	return filteredArray;
  },
  
  getErrorJson: function(message){
    if(message == "invalid json"){
      message = "JSON parsing failed";
    }
	
    json = {
      error: "Could not decode request: " + message
    };
    
    return json;
  }
};
module.exports = mi9;