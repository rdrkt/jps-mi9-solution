var express = require("express");
var bodyParser = require("body-parser");
var logfmt = require("logfmt");

var mi9 = require("./mi9.js");

var app = express();

app.use(logfmt.requestLogger());
app.use(bodyParser.json());

app.post('/', function(req, res) {
  if(!req.body['payload']){
    res.json(400, mi9.getErrorJson("Payload not found"));
  }
	
  var parsed = mi9.filterShows(
    req.body.payload, 
    function(show){ 
      return (show.drm == true && show.episodeCount > 0); 
    },
    function(show){ 
      return {
        "title": show.title,
        "slug": show.slug,
        "image": show.image.showImage
      };
    }
  );
  
  res.json({
    "response": parsed
  });
});

//production error handler
//no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.json(400, mi9.getErrorJson(err.message));
});

var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
  console.log("Listening on " + port);
});

